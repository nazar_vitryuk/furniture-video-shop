import {FormGroup, FormControl, Validators} from '@angular/forms';
import {AsyncValidator} from "../shared/async.validator";
import {Injectable} from "@angular/core";





@Injectable({
    providedIn: 'root'
})


export class SignUpForm {

    constructor(private asyncValid: AsyncValidator){}



    passwordMinLength = 6


    getSignUpForm() {
        return new FormGroup({
            'username': new FormControl(null, Validators.required),
            'surname': new FormControl(null, Validators.required),
            'phone': new FormControl(null, Validators.required, this.asyncValid.registerEmailPhoneValidator()),
            'email': new FormControl(null, [Validators.required, Validators.email], this.asyncValid.registerEmailPhoneValidator()),
            'password': new FormControl(null, [Validators.required, Validators.minLength(6)]),
            'password-repeat': new FormControl(null, [Validators.required, Validators.minLength(6)])
        }, {validators: this.checkPasswords})
    }

    checkPasswords(group: FormGroup) { // here we have the 'passwords' group
        let pass = group.controls.password.value;
        let confirmPass = group.controls['password-repeat'].value;

        console.log(pass);
        console.log(confirmPass);

        return pass === confirmPass ? null : { notSame: true }
    }
}