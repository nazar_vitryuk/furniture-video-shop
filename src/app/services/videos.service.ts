import { OnInit, Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Video } from '../shared/video.interface';


@Injectable()
export class VideoService  {

    constructor(private http: HttpClient) {}

    videoList: any
    videoSelected = new EventEmitter<Video>()
    videosReady = new EventEmitter<any>()
    originUrl = 'https://localhost:4200'

    renderVideos() {
       if (!sessionStorage.getItem("previews")) {
        let rootUrl = "https://furniture.grassbusinesslabs.ml/api";
        this.http.get(`${rootUrl}/preview`)
        .subscribe(
            (videos: any) => {
                this.videoList = videos.youtube;
                this.videosReady.emit();
                sessionStorage.setItem("previews", JSON.stringify(this.videoList));
            }
        )
       } else {
        let videos = sessionStorage.getItem("previews");
        this.videoList = JSON.parse(videos);
        this.videosReady.emit();
       }
    }

    makeUrlFromId(id) {
        return `https://www.youtube.com/embed/${id}?autoplay=1&origin=${this.originUrl}`
    }

    getFirstVideo() {
        return {
            title: this.videoList[0].snippet.title,
            url: this.makeUrlFromId(this.videoList[0].snippet.resourceId.videoId)
        }
    }

    getVideos() {
        return this.videoList
    }



}