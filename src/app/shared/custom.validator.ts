import {AbstractControl, FormGroup, ValidatorFn} from '@angular/forms';


const passMatch: ValidatorFn = (fg: FormGroup) => {
    const pass = fg.get('password').value;
    const passRep = fg.get('password-repeat').value;
    if (pass === passRep) {
        return {passNotMatch: true};
    }

    return null;

};